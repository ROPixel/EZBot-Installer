@echo off
cd %~dp0

GrfCL.exe -makeGrf tmpconfig.grf Pixel\data -breakOnExceptions true
GrfCL.exe -merge data.grf tmpconfig.grf -breakOnExceptions true

set /p DUMMY=Hit ENTER to continue...